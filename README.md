# Netify Agent Processor Plugin Template
Copyright &copy; 2024-2024 eGloo Incorporated

## Overview

This is a template project for writing new Netify Agent Processor Plugins.

## Documentation

Documentation can be found [here](doc/Netify%20Processor%20Template.md).
