// Netify Agent Template Processor
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>

#pragma once

#include <nd-plugin.hpp>

class nppPlugin : public ndPluginProcessor
{
public:
    nppPlugin(const std::string &tag, const ndPlugin::Params &params);
    virtual ~nppPlugin();

    virtual void *Entry(void);

    virtual void GetVersion(std::string &version);

    virtual void GetStatus(nlohmann::json &status);
    virtual void DisplayStatus(const nlohmann::json &status);

    virtual void DispatchEvent(ndPlugin::Event event,
      void *param = nullptr);

    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndFlowMap *flow_map);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndFlow::Ptr &flow);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndInterfaces *interfaces);
    virtual void DispatchProcessorEvent(ndPluginProcessor::Event event,
      const std::string &iface, ndPacketStats *stats);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndPacketStats *stats);
    virtual void DispatchProcessorEvent(
      ndPluginProcessor::Event event, ndInstanceStatus *status);
    virtual void
    DispatchProcessorEvent(ndPluginProcessor::Event event);

protected:
    std::atomic<bool> reload;

    void Reload(void);

    nlohmann::json status;
    std::mutex status_mutex;
};
