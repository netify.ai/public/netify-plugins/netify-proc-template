// Netify Agent Template Processor
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General
// Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>

#include <fstream>

#include <nd-except.hpp>

#include "npp-plugin.hpp"

using namespace std;
using json = nlohmann::json;

nppPlugin::nppPlugin(const string &tag, const ndPlugin::Params &params)
  : ndPluginProcessor(tag, params) {
    if (conf_filename.empty()) {
        throw ndExceptionSystemErrno(tag.c_str(),
          "conf_filename", EINVAL);
    }

    reload = true;

    nd_dprintf("%s: initialized\n", tag.c_str());
}

nppPlugin::~nppPlugin() {
    Join();

    nd_dprintf("%s: destroyed\n", tag.c_str());
}

void *nppPlugin::Entry(void) {
    nd_printf(
      "%s: %s v%s Copyright (C) 2021-2024 eGloo "
      "Incorporated.\n",
      tag.c_str(), PACKAGE_NAME, PACKAGE_VERSION);

    while (! ShouldTerminate()) {
        if (reload.load()) {
            Reload();
            reload = false;
        }

        // TODO: Do plugin processing here...
        // For a full implementation example, see:
        // https://gitlab.com/netify.ai/public/netify-plugins/netify-proc-legacy
        sleep(1);
    }

    return NULL;
}

void nppPlugin::GetVersion(string &version) {
    version = PACKAGE_VERSION;
}

void nppPlugin::GetStatus(nlohmann::json &status) {
#if 0
    lock_guard<mutex> lg(status_mutex);

    status["license_status"] = nlm.GetLicenseStatus(license_status);
    status["license_status_id"] = license_status;

    status["aggregator"] = aggregator;
    status["samples"] = samples.size();
#endif
}

void nppPlugin::DisplayStatus(const nlohmann::json &status) {
#if 0
    unsigned a = 0, s = 0;

    auto i = status.find("license_status_id");
    if (i != status.end() && i->is_number_unsigned())
        nlm.DisplayLicenseStatus(i->get<unsigned>());

    i = status.find("aggregator");
    if (i != status.end() && i->is_number_unsigned())
        a = i->get<unsigned>();

    i = status.find("samples");
    if (i != status.end() && i->is_number_unsigned())
        s = i->get<unsigned>();

    fprintf(stdout, "%s aggregator #%u\n", ndTerm::Icon::INFO, a);
    fprintf(stdout, "%s samples: %u\n", ndTerm::Icon::INFO, s);
#endif
}

void nppPlugin::DispatchEvent(ndPlugin::Event event, void *param) {
    // General plugin event handling.  Event types are defined
    // in:
    // https://gitlab.com/netify.ai/public/netify-agent/-/blob/master/include/nd-plugin.h
    switch (event) {
    case ndPlugin::Event::RELOAD: reload = true; break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndFlowMap *flow_map) {
    // Flow map update event.  The entire flow map state is
    // available to processor plugins on each update interval
    // (15s by default).  It is essential that each flow
    // bucket is locked before accessing any flows within.  It
    // is also essential to hold these locks for a little
    // amount of time possible.  It's best practice to store
    // ndFlow::Ptr objects here and then do actual processing
    // on them in the plugin's main Entry() implementation
    // above.  The ndFlow::Ptr is a shared_ptr and will stay
    // resident until all references have been released.
    switch (event) {
    case ndPluginProcessor::Event::FLOW_MAP: break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndFlow::Ptr &flow) {
    // Individual flow events.  New flow detections and
    // updated flow metadata events will be dispatched as they
    // happen.  Expiration events are dispatched are
    // dispatched by the Agent's instance thread on each
    // update interval (15s by default).
    switch (event) {
    case ndPluginProcessor::Event::FLOW_NEW: break;
    case ndPluginProcessor::Event::FLOW_EXPIRING: break;
    case ndPluginProcessor::Event::FLOW_EXPIRE: break;
    case ndPluginProcessor::Event::DPI_NEW: break;
    case ndPluginProcessor::Event::DPI_UPDATE: break;
    case ndPluginProcessor::Event::DPI_COMPLETE: break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndInterfaces *interfaces) {
    // All capture interfaces and their stats are made
    // available on each update interval.
    switch (event) {
    case ndPluginProcessor::Event::INTERFACES: break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(ndPluginProcessor::Event event,
  const string &iface, ndPacketStats *stats) {
    // Capture interfaces packet stats events are dispatched
    // on each update interval.
    switch (event) {
    case ndPluginProcessor::Event::PKT_CAPTURE_STATS: break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndPacketStats *stats) {
    // Global (summary) capture packet stats are made
    // available on each update interval.
    switch (event) {
    case ndPluginProcessor::Event::PKT_GLOBAL_STATS: break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(
  ndPluginProcessor::Event event, ndInstanceStatus *status) {
    // Indicates that an update interval has expired, and an
    // update period has begun.  Useful to synchronize state,
    // or clear/reset collected data.
    switch (event) {
    case ndPluginProcessor::Event::UPDATE_INIT: break;
    default: break;
    }
}

void nppPlugin::DispatchProcessorEvent(ndPluginProcessor::Event event) {
    // Indicates that an update period is ending.  Useful to
    // synchronize state, clear/reset collected data, or
    // dispatch a payload to a sink plugin.
    switch (event) {
    case ndPluginProcessor::Event::UPDATE_COMPLETE: break;
    default: break;
    }
}

void nppPlugin::Reload(void) {
    nd_dprintf("%s: Loading configuration: %s\n",
      tag.c_str(), conf_filename.c_str());

    json j;
    ifstream ifs(conf_filename);
    if (! ifs.is_open()) {
        Unlock();
        throw ndException(
          "%s: error loading configuration: %s: %s",
          tag.c_str(), conf_filename.c_str(), strerror(ENOENT));
    }

    try {
        ifs >> j;
    }
    catch (json::exception &e) {
        Unlock();
        throw ndException(
          "%s: error loading configuration: %s: JSON "
          "exception: %s",
          tag.c_str(), conf_filename.c_str(), e.what());
    }
    catch (exception &e) {
        Unlock();
        throw ndException(
          "%s: error loading configuration: %s: %s",
          tag.c_str(), conf_filename.c_str(), e.what());
    }

    // TODO: Load in configuration options...
}

ndPluginInit(nppPlugin);
